var colorOn = "#FC3A3A";
var colorOff = "#BFBFBF";
var binaryNumbers = [
	[0,0,0,0],
	[0,0,0,1],
	[0,0,1,0],
	[0,0,1,1],
	[0,1,0,0],
	[0,1,0,1],
	[0,1,1,0],
	[0,1,1,1],
	[1,0,0,0],
	[1,0,0,1],
	[1,0,1,0],
	[1,0,1,1],
	[1,1,0,0],
	[1,1,0,1],
	[1,1,1,0],
	[1,1,1,1]
];

function value(id, numericValue, numbers, startBit, endBit) {
	for (bit = startBit; bit <= endBit; bit++) {
		if (numbers[numericValue][bit]) {
			document.getElementById(id + "-" + bit).style.backgroundColor = colorOn;
		}
		else {
			document.getElementById(id + "-" + bit).style.backgroundColor = colorOff;
		}
	}
}

function binaryTime() {
	var time = new Date().toLocaleTimeString('en-US', { hour12: false, hour: "numeric", minute: "numeric", second: "numeric"});
	time = time.split(":").join("");
	time = time.split("");
	time = time.reverse();
	for (digit = 0; digit < time.length; digit++) {
		value(digit, time[digit], binaryNumbers, 0, 3);
	}
}

function digitalTime() {
	var time = new Date().toLocaleTimeString('en-US', { hour12: false, hour: "numeric", minute: "numeric", second: "numeric"});
	time = time.split(":").join("");
	time = time.split("");
	time = time.reverse();
	for (digit = 0; digit < time.length; digit++) {
		value(digit, time[digit], digitalNumbers, 0, 14);
	}
}

function main() {
	window.setInterval(binaryTime, 500);
}

main();
